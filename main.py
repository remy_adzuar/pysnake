import pygame
import random

pygame.init()


def main(config):

    screen = pygame.display.set_mode((1020,768))
    clock = pygame.time.Clock()

    turn = 0
    game = True
    pause = True

    objectif = config["objectif"]
    nombre_obstacle = config["obstacles"]
    speed = config["speed"]

    matrix = {"content":list(),
            "width":30,
            "height":22
            }
    matrix["content"] += [None]*matrix["width"]*matrix["height"]

    direction = ["E",[0,1]]
    apple = random_pos(matrix)

    snake = [random_pos(matrix)]



    obstacles = create_obstacles(matrix, nombre_obstacle)


    while game:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_w and direction[0] != "E":
                    direction = ["O",[0,-1]]
                if event.key == pygame.K_s and direction[0] != "O":
                    direction = ["E",[0,1]]
                if event.key == pygame.K_a and direction[0] != "S":
                    direction = ["N",[-1,0]]
                if event.key == pygame.K_d and direction[0] != "N":
                    direction = ["S",[1,0]]
                if event.key == pygame.K_SPACE:
                    if pause:
                        pause = False
                    else:
                        pause = True


        if not pause:
            turn += 1
            if turn == 1200:
                turn = 0

            if turn%speed == 0:
                #check collide
                head = snake[len(snake)-1]
                if (head == apple):
                    apple = eat_apple(snake, apple, matrix)
                    if len(snake)-1 == config["objectif"]:
                        print("Vous avez remplie votre objectif, niveau suivant!")
                        return True

                move_snake(snake, direction)

                if not check_self_bite(snake):
                    print("Snake c'est mordu lui meme")
                    return False
                if not check_collide_border(snake):
                    print("Snake a heurte une bordure")
                    return False
                if not check_collide_obstacle(snake, obstacles):
                    print("Snake a heurte un obstacle")
                    return False
                    

        draw_map(matrix, screen)
        draw_apple(apple, screen)
        print_snake(snake, screen)
        message_box(str(len(snake)-1), (980,64), screen)

        pygame.display.update()
        clock.tick(60)

    
    return True

def move_snake(snake, direction):
    last = len(snake)-1
    head = [snake[last][0],snake[last][1]]
    snake.append(head)
    snake.pop(0)
    snake[last][0] += direction[1][0]
    snake[last][1] += direction[1][1]
    print(snake)

def get_matrix_case(matrix,pos):
    mat = matrix["content"]
    width = matrix["width"]
    
    calc = pos[1]*width+pos[0]
    if calc < len(mat):
        return mat[pos[1]*width+pos[0]]
    return None

def set_matrix_case(matrix, pos, value):
    mat = matrix["content"]
    width = matrix["width"]
    mat[pos[1]*width+pos[0]] = value

def draw_map(matrix, screen):

    mat = matrix["content"]

    y = 0
    while (y < matrix["height"]):
        x = 0
        while (x < matrix["width"]):
            if get_matrix_case(matrix,(x,y)) == None:
                pygame.draw.rect(screen, (0,255,0), (x*32, y*32,32,32))
            if get_matrix_case(matrix,(x,y)) == "stone":
                pygame.draw.rect(screen, (100,100,100), (x*32, y*32,32,32))
            x+=1
        y+=1

def draw_rect_pos(pos, color, screen):
    x = pos[0]*32
    y = pos[1]*32
    pygame.draw.rect(screen, color, (x,y,32,32))

def print_snake(snake, screen):

    for pos in snake:
        draw_rect_pos(pos,(0,0,255), screen)

def random_pos(matrix, snake=None):
    while True:

        i = random.randrange(30)
        j = random.randrange(22)

        # Check avec la matrix
        if get_matrix_case(matrix, (i,j)) == None:

            if snake != None:
                last = snake[len(snake)-1]
                if last[0] != i and last[1] != j:
                    return [i,j]
            else:
                return [i,j]

def random_pos_apple(matrix, snake):
    while True:
        pos = random_pos(matrix, snake)
        north = get_matrix_case(matrix, [pos[0],pos[1]-1])
        south = get_matrix_case(matrix, [pos[0],pos[1]+1])
        east = get_matrix_case(matrix, [pos[0]+1,pos[1]])
        west = get_matrix_case(matrix, [pos[0]-1,pos[1]])
        cpt = 0
        if north != None:
            cpt+=1
        if south != None:
            cpt+=1
        if east != None:
            cpt+=1
        if west != None:
            cpt+=1
        if cpt <= 2:
            return pos


def draw_apple(apple, screen):
    draw_rect_pos(apple, (255,0,0), screen)

def eat_apple(snake,apple,matrix):
    snake.append(apple)
    apple = random_pos_apple(matrix, snake)
    return apple

def check_self_bite(snake):
    last = snake[len(snake)-1]
    i = 0
    while (i< len(snake)-2):
        x = snake[i][0]
        y = snake[i][1]
        if last[0] == x and last[1] == y:
            return False
        i+= 1
    return True

def check_collide_border(snake):
    last = snake[len(snake)-1]
    if last[0] < 0 or last[0] == 30:
        return False
    if last[1] < 0 or last[1] == 22:
        return False
    return True

def create_obstacles(matrix, number):
    obstacles = list()
    i = 0
    while (i < number):
        obstacles.append(random_pos(matrix))
        i+= 1
    for obs in obstacles:
        set_matrix_case(matrix, obs, "stone")
    return obstacles

def check_collide_obstacle(snake, obstacles):
    last = snake[len(snake)-1]
    for obs in obstacles:
        if last[0] == obs[0] and last[1] == obs[1]:
            return False
    return True

def message_box(text,pos,screen):
    white = (255,255,255)
    grey = (100,100,100)
    font = pygame.font.Font("freesansbold.ttf", 32)
    text = font.render(text, True, white, grey)
    textRect = text.get_rect()
    textRect.center = (pos[0],pos[1])
    screen.blit(text, textRect)



if __name__ == "__main__":


    levels = [
        {"objectif":25,
    "description":"1, objectif 25 pommes",
    "obstacles": 10,
    "speed":15},
        {"objectif":50,
    "description":"2, objectif 50 pommes",
    "obstacles":20,
    "speed":10},
        {"objectif":100,
    "description":"3, objectif 100 pommes",
    "obstacles":40,
    "speed":6}
    ]

    level = 0
    alive = True
    while alive:
        niveau = levels[level]

        print("Vous etes niveau : ",niveau["description"])
        input("Appuyez sur une touche pour commencer...")
        alive = main(niveau)

        level += 1
        if level == 3:
            alive = False
        
    print("!Au Revoir!")