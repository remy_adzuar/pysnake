Projet PySnake

Faire un Snake avec python3 et pygame

1 session de 1h30 pour faire le proto
1 session de 1h30 pour refactoriser et polish

1 session de 5min sur papier pour preparer la feuille de route
1 session de 5min sur ordinateur pour mettre en ordre le projet et rediger ce document

Fonctionalites :

- Snake se deplace sur une grille 2D
- Snake grossit en mangeant des pommes
- Snake meurt en se mordant lui meme
- Snake meurt en heurtant un des bords du jeu
- Snake meurt en mordant un obstacle

Idees :

- Faire bouger les pommes (ou ajouter d'autres enemis)
- Faire planer un rapace dangereux pour Snake (ou les proies)
- Augmenter la vitesse au fur et a mesure du jeu
- Ajouter des trous de taupes (qui warpent Snake ailleur)
- Mettre des objectifs de victoire

Objectif :

Avoir un jeu jouable et fini a presenter sur Github.

Pre requis :

Sous linux, si besoin telecharger python3-pip et faire un pip install pygame


Suivis :

1 ere session
25 min, Blocage sur la matrice
5 min, pause
25 min, Matrice ok, Mouvement Snake en cours
5 min, pause
25 min, Mouvement Snake ok, Ajout pomme en position random ok, reste a implementer les morts de snake

Ajout d'une inter session de 25min pour terminer le proto

inter session
25min, Les morts (collisions) sont OK, reste a paufiner

2 eme session

25 min, Suppression variable globales, creation d'un dictionnaire matrix, Besoin de controler qu'une pomme ne tombe pas sur une pierre
5 min, pause
25 min, Ajout nombre de pierre a placer aleatoirement, Reflexion sur le nombre de niveau, trois ou quatre => verdict 3. Ajouter un verif sur la position de depart et sur le fait de ne pas coincer une pomme entre 3 pierre

coupure

25 min, Finition du jeu, Affichage du score en Jeu, Fichier de config pour gerer la difficulte des niveaux

Point a ameliorer :

Beaucoup de dette technique, la refactorisation a apporte beaucoup de bien. Faire plus de dictionnaire et d'object dans les prochain projet, voir une class singleton
Ajout de test pour les operation sur les matrices qui sont delicates a debugger autrement, ainsi que sur les detection de collision

Plus (+) tirer parti des matrices pour detecter les collisions des objets dans la scene, dans ce projet snake est trop independant de la matrice par exemple